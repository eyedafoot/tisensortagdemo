// JavaScript code for the TI SensorTag Demo app.

/**
 * Holds a list of devices
 */
var devices = [];
/**
 * Object that holds application data and functions.
 */
var app = {};

/**
 * Data that is plotted on the canvas.
 */
app.dataPoints = [];


/**
 * Timeout (ms) after which a message is shown if the SensorTag wasn't found.
 */
app.CONNECT_TIMEOUT = 5000;

/**
 * Object that holds SensorTag UUIDs.
 */
app.sensortag = {};

// UUIDs for movement services and characteristics.
app.sensortag.MOVEMENT_SERVICE = 'f000aa80-0451-4000-b000-000000000000';
//app.sensortag.MOVEMENT_DATA = 'f000aa81-0451-4000-b000-000000000000';
app.sensortag.MOVEMENT_CONFIG = 'f000aa82-0451-4000-b000-000000000000';
app.sensortag.MOVEMENT_PERIOD = 'f000aa83-0451-4000-b000-000000000000';
app.sensortag.MOVEMENT_NOTIFICATION = '00002902-0000-1000-8000-00805f9b34fb';

// MY UUIDs for movement services and characteristics.
app.sensortag.MY_MOVEMENT_DATA = 'f000ff81-0451-4000-b000-000000000000';
app.sensortag.MY_MOVEMENT_DATA_NUM_SAMPLES = 'f000ff84-0451-4000-b000-000000000000';

/**
 * Initialise the application.
 */
app.initialize = function()
{
	document.addEventListener(
		'deviceready',
		function() { evothings.scriptsLoaded(app.onDeviceReady) },
		false);

	// Called when HTML page has been loaded.
	$(document).ready( function()
	{
		// Adjust canvas size when browser resizes
		$(window).resize(app.respondCanvas);

		// Adjust the canvas size when the document has loaded.
		app.respondCanvas();
	});
};

/**
 * Adjust the canvas dimensions based on its container's dimensions.
 */
app.respondCanvas = function()
{
	var canvas = $('.canvas')
	var container = $(canvas).parent()
	canvas.attr('width', $(container).width() ) // Max width
	// Not used: canvas.attr('height', $(container).height() ) // Max height
};

app.onDeviceReady = function()
{
	app.showInfo('Activate the SensorTag and tap Start.');
};

app.showInfo = function(info)
{
	document.getElementById('info').innerHTML = info;
};

app.onStartButton = function()
{
	//app.onStopButton();
	app.startScan();
	app.showInfo('Status: Scanning...');
	app.startConnectTimer();
};

app.onStopButton = function()
{
	// Stop any ongoing scan and close devices.
	app.stopConnectTimer();
	evothings.easyble.stopScan();
	evothings.easyble.closeConnectedDevices();
	app.showInfo('Status: Stopped.');
	
	$('#graphs').empty();
	devices = [];
};

app.startConnectTimer = function()
{
	// If connection is not made within the timeout
	// period, an error message is shown.
	app.connectTimer = setTimeout(
		function()
		{
			app.showInfo('Status: Scanning... ' +
				'Please press the activate button on the tag.');
		},
		app.CONNECT_TIMEOUT)
}

app.stopConnectTimer = function()
{
	clearTimeout(app.connectTimer);
}

app.startScan = function()
{
	evothings.easyble.reportDeviceOnce(true);
	evothings.easyble.startScan(
		function(device)
		{
			// Connect if we have found a sensor tag.
			if (app.deviceIsSensorTag(device))
			{
				app.showInfo('Status: Device found: ' + device.name + '.');
				evothings.easyble.stopScan();
				app.addDeviceToAcceptanceQueue(device);
			}
		},
		function(errorCode)
		{
			app.showInfo('Error: startScan: ' + errorCode + '.');
		});
};

app.continueStartScan1 = function (device) 
{
	app.checkServerForNodeList(device)
}

app.continueStartScan2 = function (device)
{
	app.connectToDevice(device);
	app.stopConnectTimer();
}

app.addDeviceToAcceptanceQueue = function(device)
{
	console.log('addDeviceToAcceptanceQueue: ' + device.address);
	app.log('addDeviceToAcceptanceQueue: ' + device.address);
	
	alreadyExists = false;
	
	if(typeof devices !== 'undefined' && devices.length > 0) 
	{
		for(var i = 0; i < devices.length; i++)
		{
			alreadyExists = (device.address == devices[i].address);
		}
	}
	
	if (!alreadyExists)
	{
		var serverIP = $('#serverIP').val();
		var masterKey = $('#masterKey').val();
		var gatewayId = $('#gatewayId').val();
		var url = 'http://'+serverIP+':3000/mobileapi/devices/approvalrequest.json';
		console.log('url:' + url);
		
		cordovaHTTP.post(url, {"master_key":masterKey, "gateway_id":gatewayId, "address":device.address}, {},
						function(response)
						{
							console.log('Success');
							app.continueStartScan1(device);
						},
						function(response)
						{
							if (response != null || response != 'undefined')
							{
								data = JSON.parse(response.data);
								console.log(data.error);
								app.log(data.error);
							}
						});
	}
}

app.checkServerForNodeList = function(device)
{
	console.log('checkServerForNodeList: ' + device.address);
	app.log('checkServerForNodeList: ' + device.address);
	
	var serverIP = $('#serverIP').val();
	var masterKey = $('#masterKey').val();
	var gatewayId = $('#gatewayId').val();
	var url = 'http://'+serverIP+':3000/mobileapi/devices.json';
	console.log('url:' + url);
	
	var registered = false;
	
	cordovaHTTP.get(url, {"master_key":masterKey, "gateway_id":gatewayId}, {},
					function(response)
					{
						data = JSON.parse(response.data);
						console.log('Success');
						for(var i = 0; i < data.length; i++)
						{
							console.log('device['+i+'] address: ' + data[i].address);
							console.log('device address: ' + device.address);
							app.log('device['+i+'] address: ' + data[i].address);
							app.log('device address: ' + device.address);
							if (device.address == data[i].address)
							{
								registered = true;
								//Device field types and field ids
								device.fields = {accel:{accel_x:-1,accel_y:-1,accel_z:-1},gyro:{gyro_x:-1,gyro_y:-1,gyro_z:-1}};
								for(var j = 0; j < data[i].fields.length; j++)
								{
									switch(data[i].fields[j].field_type)
									{
										case 'x-accel':
											device.fields.accel.accel_x = data[i].fields[j].id;
											break;
										case 'y-accel':
											device.fields.accel.accel_y = data[i].fields[j].id;
											break;
										case 'z-accel':
											device.fields.accel.accel_z = data[i].fields[j].id;
											break;
										case 'x-gyro':
											device.fields.gyro.gyro_x = data[i].fields[j].id;
											break;
										case 'y-gyro':
											device.fields.gyro.gyro_y = data[i].fields[j].id;
											break;
										case 'z-gyro':
											device.fields.gyro.gyro_z = data[i].fields[j].id;
											break;
										default:
											break;
									}
								}
							}
						}
						if (registered)
						{
							app.continueStartScan2(device);
						}
					},
					function(response)
					{
						data = JSON.parse(response.data);
						console.log(data.error);
						app.log(data.error);
					});
	return registered
}

app.deviceIsSensorTag = function(device)
{
	console.log('device name: ' + device.name);
	return (device != null) &&
		(device.name != null) &&
		(device.name.indexOf('Sensor Tag') > -1 ||
			device.name.indexOf('SensorTag') > -1);
};

/**
 * Read services for a device.
 */
app.connectToDevice = function(device)
{
	app.showInfo('Connecting...');
	device.connect(
		function(device)
		{
			app.showInfo('Status: Connected - reading SensorTag services...');
			app.addDeviceToList(device);
			app.readServices(device);
		},
		function(errorCode)
		{
			app.showInfo('Connection error: ' + errorCode);
		});
};

app.addDeviceToList = function(device)
{
	devices.push(device);
	device.deviceId = devices.length;
	console.log('Device Count: ' + devices.length);
	
	//Device data arrays
	device.dataPoints = {};
	device.dataPoints.acc = [];
	device.dataPoints.gyro = [];
	
	var graphsDiv = $('#graphs');
	graphsDiv.append('<canvas id="canvas'+device.deviceId+'" class="canvas" width="300" height="150"></canvas>');
	graphsDiv.append('<canvas id="canvas'+device.deviceId+'gyro" class="canvas" width="300" height="150"></canvas>');
}

app.readServices = function(device)
{
	device.readServices(
		[
		app.sensortag.MOVEMENT_SERVICE // Movement service UUID.
		],
		// Function that monitors accelerometer data.
		app.startAccelerometerNotification,
		function(errorCode)
		{
			console.log('Error: Failed to read services: ' + errorCode + '.');
		});
};

/**
 * Read accelerometer data.
 */
app.startAccelerometerNotification = function(device)
{
	app.showInfo('Status: Starting accelerometer notification...');
	// Set accelerometer configuration to ON.
	// magnetometer on: 64 (1000000) (seems to not work in ST2 FW 0.89)
	// 3-axis acc. on: 56 (0111000)
	// 3-axis gyro on: 7 (0000111)
	// 3-axis acc. + 3-axis gyro on: 63 (0111111)
	// 3-axis acc. + 3-axis gyro + magnetometer on: 127 (1111111)
	device.writeCharacteristic(
		app.sensortag.MOVEMENT_CONFIG,
		new Uint8Array([63,0]),
		function()
		{
			console.log('Status: writeCharacteristic ok. - MOVEMENT_CONFIG');
			app.log('Status: writeCharacteristic ok. - MOVEMENT_CONFIG');
		},
		function(errorCode)
		{
			console.log('Error: writeCharacteristic: ' + errorCode + '.');
			app.log('Error: writeCharacteristic: ' + errorCode + '.');
		});
		
	// Set accelerometer period to 10 ms.
	device.writeCharacteristic(
		app.sensortag.MOVEMENT_PERIOD,
		new Uint8Array([8]),
		function()
		{
			console.log('Status: writeCharacteristic ok. - MOVEMENT_PERIOD');
			app.log('Status: writeCharacteristic ok. - MOVEMENT_PERIOD');
		},
		function(errorCode)
		{
			console.log('Error: writeCharacteristic: ' + errorCode + '.');
			app.log('Error: writeCharacteristic: ' + errorCode + '.');
		});
		
	device.writeCharacteristic(
		app.sensortag.MY_MOVEMENT_DATA_NUM_SAMPLES,
		new Uint8Array([2]),
		function()
		{
			console.log('Status: writeCharacteristic ok. - MY_MOVEMENT_DATA_NUM_SAMPLES');
			app.log('Status: writeCharacteristic ok. - MY_MOVEMENT_DATA_NUM_SAMPLES');
		},
		function(errorCode)
		{
			console.log('Error: writeCharacteristic: ' + errorCode + '.');
			app.log('Error: writeCharacteristic: ' + errorCode + '.');
		});

	// Set accelerometer notification to ON.
	device.writeDescriptor(
		//app.sensortag.MOVEMENT_DATA,
		app.sensortag.MY_MOVEMENT_DATA,
		app.sensortag.MOVEMENT_NOTIFICATION, // Notification descriptor.
		new Uint8Array([1,0]),
		function()
		{
			console.log('Status: writeDescriptor ok. - MY_MOVEMENT_DATA, MOVEMENT_NOTIFICATION');
			app.log('Status: writeDescriptor ok. - MY_MOVEMENT_DATA, MOVEMENT_NOTIFICATION');
		},
		function(errorCode)
		{
			// This error will happen on iOS, since this descriptor is not
			// listed when requesting descriptors. On iOS you are not allowed
			// to use the configuration descriptor explicitly. It should be
			// safe to ignore this error.
			console.log('Error: writeDescriptor: ' + errorCode + '.');
			app.log('Error: writeDescriptor: ' + errorCode + '.');
		});

	// Start accelerometer notification.
	/*
	device.enableNotification(
		app.sensortag.MOVEMENT_DATA,
		function(data)
		{
			app.showInfo('Status: Data stream active - accelerometer');
			app.showInfo(data);
			var dataArray = new Uint8Array(data);
			var values = app.getAccelerometerValues(dataArray);
			//app.drawDiagram(values);
			app.drawDiagram(values, 'canvas', 'timestamp');
			//var gyroValues = app.getGyroValues(dataArray);
			//app.drawDiagram({x:values.x, y:values.y, z:values.z, gx:gyroValues.x, gy:gyroValues.y, gz:gyroValues.z});
		},
		function(errorCode)
		{
			console.log('Error: enableNotification: ' + errorCode + '.');
		});
	*/
	device.enableNotification(
		app.sensortag.MY_MOVEMENT_DATA,
		function(data)
		{
			app.showInfo('Status: My Data stream active - accelerometer and gyro');
			//app.showInfo(data);
			var dataArray = new Uint8Array(data);
			//app.showInfo(dataArray);
			var accelValues = app.getAccelerometerValues(dataArray);
			app.drawDiagram(accelValues, device.deviceId, false);
			var gyroValues = app.getGyroValues(dataArray);
			app.drawDiagram(gyroValues, device.deviceId, true);
			if ($('input:radio[name="sendData"]:checked').val() > 0)
			{
				app.sendData(device, accelValues, false);
				app.sendData(device, gyroValues, true);
			}
		},
		function(errorCode)
		{
			console.log('Error: enableNotification: ' + errorCode + '.');
			app.log('Error: enableNotification: ' + errorCode + '.');
		});
};

/**
 * Calculate accelerometer values from raw data for SensorTag 2.
 * @param data - an Uint8Array.
 * @return Object with fields: x, y, z.
 */
app.getAccelerometerValues = function(data)
{
	var divisors = { x: -16384.0, y: 16384.0, z: -16384.0 };

	// Calculate accelerometer values.
	var ax = evothings.util.littleEndianToInt16(data, 6) / divisors.x;
	var ay = evothings.util.littleEndianToInt16(data, 8) / divisors.y;
	var az = evothings.util.littleEndianToInt16(data, 10) / divisors.z;

	// Return result.
	return { x: ax, y: ay, z: az };
};

/**
 * Calculate gyro values from raw data for SensorTag 2.
 * @param data - an Uint8Array.
 * @return Object with fields: x, y, z.
 */
app.getGyroValues = function(data)
{
	var divisors = { x: -(65536 / 500), y: (65536 / 500), z: -(65536 / 500) };

	var x = evothings.util.littleEndianToInt16(data, 0);
	var y = evothings.util.littleEndianToInt16(data, 2);
	var z = evothings.util.littleEndianToInt16(data, 4);
	
	// Calculate accelerometer values.
	var ax = evothings.util.littleEndianToInt16(data, 0)
	var ay = evothings.util.littleEndianToInt16(data, 2)
	var az = evothings.util.littleEndianToInt16(data, 4)

	// Return result.
	return { x: ax, y: ay, z: az };
};

/**
 * Plot diagram of sensor values.
 * Values plotted are expected to be between -1 and 1
 * and in the form of objects with fields x, y, z.
 */
 
 var numSamples = 0;
 var startTime = Date.now();
 
app.drawDiagram = function(values, deviceId, gyro)
{
	if (devices.length <= 0 || devices[0] == 'undefined')
	{
		return;
	}
	
	var canvas = document.getElementById('canvas'+deviceId);
	if (gyro)
	{
		canvas = document.getElementById('canvas'+deviceId+'gyro');
	}
	var context = canvas.getContext('2d');

	// Add recent values.
	var device = devices[deviceId-1];
	if (gyro)
	{
		device.dataPoints.gyro.push(values);
	}
	else
	{
		device.dataPoints.acc.push(values);
	}

	// Remove data points that do not fit the canvas.
	if (device.dataPoints.gyro.length > canvas.width && gyro)
	{
		device.dataPoints.gyro.splice(0, (device.dataPoints.gyro.length - canvas.width));
	}
	else if (device.dataPoints.acc.length > canvas.width)
	{
		device.dataPoints.acc.splice(0, (device.dataPoints.acc.length - canvas.width));
	}

	// Value is an accelerometer reading between -1 and 1.
	function calcDiagramY(value, canvas)
	{
		// Return Y coordinate for this value.
		var diagramY =
			((value * canvas.height) / 2)
			+ (canvas.height / 2);
		return diagramY;
	}
	
	function drawLine(axis, color, canvas, context, device, gyro)
	{
		context.strokeStyle = color;
		context.beginPath();
		if (gyro)
		{
			var lastDiagramY = calcDiagramY(
				(device.dataPoints.gyro[device.dataPoints.gyro.length-1][axis]/10000),
				canvas);
			context.moveTo(0, lastDiagramY);
			var x = 1;
			for (var i = device.dataPoints.gyro.length - 2; i >= 0; i--)
			{
				var y = calcDiagramY((device.dataPoints.gyro[i][axis]/10000), canvas);
				context.lineTo(x, y);
				x++;
			}
		}
		else
		{
			var lastDiagramY = calcDiagramY(
				device.dataPoints.acc[device.dataPoints.acc.length-1][axis],
				canvas);
			context.moveTo(0, lastDiagramY);
			var x = 1;
			for (var i = device.dataPoints.acc.length - 2; i >= 0; i--)
			{
				var y = calcDiagramY(device.dataPoints.acc[i][axis], canvas);
				context.lineTo(x, y);
				x++;
			}

		}
		context.stroke();
	}

	// Clear background.
	context.clearRect(0, 0, canvas.width, canvas.height);

	// Draw lines.
	if (gyro)
	{
		drawLine('x', '#c0c', canvas, context, device, true);
		drawLine('y', '#ff0', canvas, context, device, true);
		drawLine('z', '#3cc', canvas, context, device, true);
	}
	else
	{
		drawLine('x', '#f00', canvas, context, device, false);
		drawLine('y', '#0f0', canvas, context, device, false);
		drawLine('z', '#00f', canvas, context, device, false);
	}
};


/**
 * Send Data to server to be stored
 * device: device sending data
 * data: format of data {x: x-val, y: y-val, z:z-val}
 */
 
 var dataToSend = [];
 var dataFailedToSend = [];
 var lastSentTime = (new Date().getTime()/1000);
 
app.sendData = function(device, data, gyro)
{	
	if (gyro)
	{
		if (device.fields.gyro.gyro_x > -1) {dataToSend.push({field_id:device.fields.gyro.gyro_x,value:data.x,created_at:new Date()})};
		if (device.fields.gyro.gyro_y > -1) {dataToSend.push({field_id:device.fields.gyro.gyro_y,value:data.y,created_at:new Date()})};
		if (device.fields.gyro.gyro_z > -1) {dataToSend.push({field_id:device.fields.gyro.gyro_z,value:data.z,created_at:new Date()})};	
	}
	else
	{
		if (device.fields.accel.accel_x > -1) {dataToSend.push({field_id:device.fields.accel.accel_x,value:data.x,created_at:new Date()})};
		if (device.fields.accel.accel_y > -1) {dataToSend.push({field_id:device.fields.accel.accel_y,value:data.y,created_at:new Date()})};
		if (device.fields.accel.accel_z > -1) {dataToSend.push({field_id:device.fields.accel.accel_z,value:data.z,created_at:new Date()})};
	}
	
	var currentTime = (new Date().getTime()/1000);
	if ((currentTime - lastSentTime) >  5)
	{
		lastSentTime = currentTime;
		
		var serverIP = $('#serverIP').val();
		var masterKey = $('#masterKey').val();
		var url = 'http://'+serverIP + ':3000/mobileapi/data.json';
		
		var totalData = dataFailedToSend;
		totalData.push.apply(totalData, dataToSend);
		var finalData = {data:totalData, master_key:masterKey};
		dataFailedToSend = dataToSend;
		dataToSend = [];
		
		console.log(JSON.stringify(finalData));
		
		
		
		cordovaHTTP.post(url, finalData, {},
						function(response){
							dataFailedToSend = [];
						},
						function(response) {
							try {
								data = JSON.parse(response.data);
								console.log(data.error);
							} catch (err) {
								console.log('Save Data Failed');
							}
						});
		
	}
}

app.log = function (message) {
		var logger = $('#logger');
        if (typeof message == 'object') {
            logger.append((JSON && JSON.stringify ? JSON.stringify(message) : message) + '<br />');
        } else {
            logger.append(message + '<br />');
        }
    }

// Initialize the app.
app.initialize();
